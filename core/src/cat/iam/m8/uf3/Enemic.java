package cat.iam.m8.uf3;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Enemic {

	final Texture textura;
	Rectangle rectangle;
	
	public Enemic(Texture textura, Rectangle posicioInicial) {
		this.textura = textura;
		this.rectangle = posicioInicial;
	}
	
	public void draw(SpriteBatch openedBatch) {
		
		openedBatch.draw(textura,  rectangle.x, rectangle.y, rectangle.width, rectangle.height);
	}

	public boolean update(float delta) {
		
		rectangle.y -= Constants.ENEMIC_VELOCITATY * delta;
		
		if (rectangle.y < 0) {
			return false;
		}
		return true;
	}

	public boolean overlaps(Rectangle personatge) {
		
		return rectangle.overlaps(personatge);
	}
	
	public float getX () {
		return rectangle.x;
	}
	
	public float getY() {
		return rectangle.y;
	}

}
