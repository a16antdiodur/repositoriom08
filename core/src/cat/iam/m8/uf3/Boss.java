package cat.iam.m8.uf3;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Boss {
	
	final Texture texturaEsquerra;
	final Texture textura;
	final Texture texturaDreta;
	Rectangle r1;
	Rectangle r2;
	Rectangle r3;
	
	public Boss(Texture texturaEsquerra, Texture textura, Texture texturaDreta, int posicioInicialX, int posicioInicialY) {
		this.texturaEsquerra = texturaEsquerra;
		this.textura = textura;
		this.texturaDreta = texturaDreta;
		
		int y = posicioInicialY + Constants.BOSS_ALT_R1;
		int width = posicioInicialX + Constants.BOSS_AMPLE_R1;
		int height = posicioInicialY + Constants.BOSS_ALT_R1;
		
		this.r1 = new Rectangle(posicioInicialX, y, Constants.BOSS_AMPLE_R1, Constants.BOSS_ALT_R1);
		
		int x = posicioInicialX + Constants.BOSS_AMPLE_R1;
		this.r2 = new Rectangle(x, posicioInicialY, Constants.BOSS_AMPLE_R2, Constants.BOSS_ALT_R2);
		
		x = x + Constants.BOSS_AMPLE_R2;
		y = posicioInicialY + Constants.BOSS_ALT_R3;
		this.r3 = new Rectangle(x, y, Constants.BOSS_AMPLE_R3, Constants.BOSS_ALT_R3);
	}
	
	public void draw(SpriteBatch openedBatch) {
		
		openedBatch.draw(texturaEsquerra, r1.x, r1.y, r1.width, r1.height);
		openedBatch.draw(textura, r2.x, r2.y, r2.width, r2.height);
		openedBatch.draw(texturaDreta, r3.x, r3.y, r3.width, r3.height);
	}
	
	public boolean update(float delta) {
		
		r1.y -= Constants.BOSS_VELOCITATY * delta;
		
		if (r1.y < 0) {
			return false;
		}
		
		r2.y -= Constants.BOSS_VELOCITATY * delta;
		
		if (r2.y < 0) {
			return false;
		}
		
		r3.y -= Constants.BOSS_VELOCITATY * delta;
		
		if (r3.y < 0) {
			return false;
		}
		
		return true;
	}
	
	public boolean overlaps(Rectangle personatge) {
		
		if(r1.overlaps(personatge)) {
			return true;
		}
		else if (r2.overlaps(personatge)) {
			return true;
		}
		else if (r3.overlaps(personatge)) {
			return true;
		}
		return false;
	}

}
