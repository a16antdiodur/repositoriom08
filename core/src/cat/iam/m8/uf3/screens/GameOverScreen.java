package cat.iam.m8.uf3.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;

import cat.iam.m8.uf3.Constants;
import cat.iam.m8.uf3.Joc;

public class GameOverScreen extends ScreenAdapter {
	
	Texture gameOver;
	final Joc joc;
	
	float tempsAcumulat;
	
	public GameOverScreen (Joc joc) {
		this.joc = joc;
		gameOver = new Texture("graphica/gameover.png");
		tempsAcumulat = 0;
	}
	
	public void render(float delta) {
		super.render(delta);
		
		tempsAcumulat += delta;
		
		if (tempsAcumulat > Constants.TEMPS_ENTRE_GAMEOVER_GAME) {
			joc.setScreen(new GameScreen(joc));
			// Al cambiar de pantalla, nosotros tenemos que llamar al dispose para cerrar recursos :)
			this.dispose();
		}
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		int width = Constants.MON_AMPLE;
		int height = Constants.MON_ALT;
		
		joc.batch.begin();

		joc.batch.draw(gameOver, 0, 0, width, height);
				
		joc.batch.end();
	}
	
	public void dispose() {
		super.dispose();
		gameOver.dispose();
	}
}
