package cat.iam.m8.uf3.screens;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import cat.iam.m8.uf3.Boss;
import cat.iam.m8.uf3.Constants;
import cat.iam.m8.uf3.Enemic;
import cat.iam.m8.uf3.Joc;

// Podemos poner extends Screen, pero hay que implementar todos los métodos, pero tenemos
// el ScreenAdapter que provien de Screen, pero sin los métodos :)
public class GameScreen extends ScreenAdapter {

	Texture enemicTex;
	Texture personatgeTex;
	Texture disparPlayerTex;
	Texture disparEnemicTex;
	Texture bossTexDreta;
	Texture bossTex;
	Texture bossTexEsquerra;
	
	Texture fons;

	Rectangle personatge;

	//List<Rectangle> enemics;

	float tempsPassat;
	float tempsPassatDisparEnemic;
	float tempsPassatNouBoss;
	
	final Joc joc;
	
	List<Rectangle> disparsPlayer;
	List<Rectangle> disparsEnemic;
	List<Enemic> enemicsSprites;
	
	List<Boss> boss;
	
	int disparsBoss = 0;
	
	public GameScreen(Joc joc) {
		
		this.joc = joc;

		personatge = new Rectangle (
				Constants.PLAYER_POSX_INICIAL,
				Constants.PLAYER_POSY_INICIAL,
				Constants.PLAYER_AMPLE,
				Constants.PLAYER_ALT);
		
		fons = new Texture ("graphica/blue.png");
		
		//personatgeTex = new Texture("graphica/badlogic.jpg");
		personatgeTex = new Texture("graphica/playerShip1_green.png");

		enemicTex = new Texture("graphica/enemyRed5.png");
		//enemics = new ArrayList<Rectangle>();
		//obstacles.add(new Rectangle(0, 500, 30, 30));
		
		// Inicialment no hi ha obstacles
		enemicsSprites = new ArrayList<Enemic>();
		tempsPassat = 0;
		
		disparPlayerTex = new Texture("graphica/laserBlue07.png");
		disparsPlayer = new ArrayList<Rectangle>();
		
		disparEnemicTex = new Texture("graphica/laserRed03.png");
		disparsEnemic = new ArrayList<Rectangle>();
		
		bossTexEsquerra = new Texture("graphica/wingRed_3Esquerra.png");
		bossTex = new Texture("graphica/cockpitRed_4.png");
		bossTexDreta = new Texture("graphica/wingRed_3.png");
		boss = new ArrayList<Boss>();

	}

	public void render(float delta) {
		super.render(delta);
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		input(delta);

		update(delta);

		//xoc();

		joc.batch.begin();
		
		//Dibuixem imatge de fons
		joc.batch.draw(fons, 0, 0, Constants.MON_AMPLE,Constants.MON_ALT);
		
		joc.batch.draw(personatgeTex, personatge.x, personatge.y, personatge.width, personatge.height);
		/*for (Rectangle enemic: enemics) {
			joc.batch.draw(enemicTex, enemic.x, enemic.y, enemic.width, enemic.height);
		}*/
		
		// Dibuixem primer els dispars dels enemics, per no solapar els dispars a sobre dels enemics
		for (Rectangle disparEnemic : disparsEnemic) {
			joc.batch.draw(disparEnemicTex, disparEnemic.x, disparEnemic.y, disparEnemic.width, disparEnemic.height);
		}
		
		for (Enemic enemic : enemicsSprites) {
			enemic.draw(joc.batch);
		}
		
		for (Rectangle disparPlayer: disparsPlayer) {
			joc.batch.draw(disparPlayerTex, disparPlayer.x, disparPlayer.y, disparPlayer.width, disparPlayer.height);
		}
		
		// Dibuixar Boss
		for (Boss boss : boss) {
			boss.draw(joc.batch);
		}
		
		joc.batch.end();
	}

	private void update(float delta) {

		// getDeltaTime nos da el tiempo en segundos desde la última vez que se dibujaron cosas en pantalla
		delta = Gdx.graphics.getDeltaTime();

		tempsPassat += delta;
		tempsPassatDisparEnemic += delta;
		tempsPassatNouBoss += delta;

		// nou enemic
		if (tempsPassat > Constants.TEMPS_NOU_ENEMIC) {

			Random rand = new Random();
			int amplePantalla = Constants.MON_AMPLE;
			int altPantalla = Constants.MON_ALT;
			int iniciX = rand.nextInt(amplePantalla-Constants.ENEMIC_AMPLE) + 1;

			//enemics.add(new Rectangle(iniciX, altPantalla, Constants.ENEMIC_AMPLE, Constants.ENEMIC_ALT));
			Rectangle posInicial = new Rectangle(iniciX, altPantalla, Constants.ENEMIC_AMPLE, Constants.ENEMIC_ALT);
			Enemic enemic = new Enemic(enemicTex, posInicial);
			enemicsSprites.add(enemic);
			
			tempsPassat = 0;
		}
		
		// nou dispar enemic
		if (tempsPassatDisparEnemic > Constants.TEMPS_NOU_DISPAR_ENEMIC) {
			if (enemicsSprites.size() > 0) {
				Random rand = new Random();
				int enemicDispara = rand.nextInt(enemicsSprites.size());
				
				float x = enemicsSprites.get(enemicDispara).getX() + Constants.ENEMIC_AMPLE / 2;
				float y = enemicsSprites.get(enemicDispara).getY();
				Rectangle disparEnemic = new Rectangle(x, y, Constants.DISPAR_ENEMIC_AMPLE, Constants.DISPAR_ENEMIC_ALT);
				disparsEnemic.add(disparEnemic);
			}
			
			tempsPassatDisparEnemic = 0;
		}

		// moure enemics
		/*for (Rectangle enemic: enemics) {
			enemic.y = enemic.y - Constants.ENEMIC_VELOCITATY * delta;
		}*/
		
		// eliminar enemics
		/*for (Iterator iterator = enemics.iterator(); iterator.hasNext();) {
			Rectangle enemic = (Rectangle) iterator.next();
			if (enemic.y < 0)
				iterator.remove();
		}*/
		// Moure enemics i comprovar si surten de la pantalla per eliminar
		for (Iterator iterator = enemicsSprites.iterator(); iterator.hasNext();) {
			Enemic enemic = (Enemic) iterator.next();
			// Fora pantalla
			if (!enemic.update(delta)) {
				iterator.remove();
			}
		}
		
		for (Iterator iterator = enemicsSprites.iterator(); iterator.hasNext();) {
			Enemic enemic = (Enemic) iterator.next();
			// Colisions amb el player
			if  (enemic.overlaps(personatge)) {
				joc.setScreen(new GameOverScreen(joc));
				this.dispose();
			}
		}
		
		// moure dispars jugador
		for (Rectangle disparPlayer : disparsPlayer) {
			disparPlayer.y += Constants.DISPAR_PLAYER_VELOCITATY * delta;
		}
		
		// moure dispars enemics
		for (int i = 0; i < disparsEnemic.size(); i++) {
			Rectangle disparEnemic = disparsEnemic.get(i);
			disparEnemic.y -= Constants.DISPAR_ENEMIC_VELOCITATY * delta;
			// fora pantalla
			if (disparEnemic.y <= 0) {
				disparsEnemic.remove(i);
			}
			// xoca enemic
			// Hem de comparar cada dispar amb el jugador (personatge)
			if (personatge.overlaps(disparEnemic)) {
				disparsEnemic.remove(i);
				joc.setScreen(new GameOverScreen(joc));
				this.dispose();
			}
		}
		
		// eliminar dispars
		for (int i = 0; i < disparsPlayer.size(); i ++) {
			Rectangle dispar = disparsPlayer.get(i);
			// fora pantalla
			if (dispar.y > Constants.MON_ALT) {
				disparsPlayer.remove(i);
			}
			// xoca enemic
			// Hem de comparar cada dispar amb tots els enemics
			for (int j = 0; j < enemicsSprites.size(); j ++) {
				Enemic enemic = enemicsSprites.get(j);
				
				if (enemic.overlaps(dispar)) {
					enemicsSprites.remove(j);
					disparsPlayer.remove(i);
				}
			}
			
			for (int j = 0; j < boss.size(); j ++) {
				Boss bossAux = boss.get(j);
				
				if (bossAux.overlaps(dispar)) {
					disparsBoss ++;
					disparsPlayer.remove(i);
					
					if (Constants.BOSS_XOC_DISPARS <= disparsBoss) {
						boss.remove(j);
						disparsBoss = 0;
					}
				}
			}
		}
		
		// Crear Boss
		if (boss.size() < Constants.BOSS_MAX && tempsPassatNouBoss >= Constants.TEMPS_NOU_BOSS) {
			int bossXSortida = Constants.MON_AMPLE / 2 - Constants.BOSS_AMPLE_R1 - (Constants.BOSS_AMPLE_R2 / 2);
			Boss bossAux = new Boss(bossTexEsquerra, bossTex, bossTexDreta, bossXSortida, Constants.MON_ALT);
			boss.add(bossAux);
			
			tempsPassatNouBoss = 0;
		}
		
		// Moure Boss i comprovar si surt de la pantalla per eliminar
		for (Iterator iterator = boss.iterator(); iterator.hasNext();) {
			Boss bossAux = (Boss) iterator.next();
			// Fora pantalla
			if (!bossAux.update(delta)) {
				iterator.remove();
			}
		}
		
		for (Iterator iterator = boss.iterator(); iterator.hasNext();) {
			Boss boss = (Boss) iterator.next();
			// Colisions amb el player
			if  (boss.overlaps(personatge)) {
				joc.setScreen(new GameOverScreen(joc));
				this.dispose();
			}
		}
	}

	private void input(float delta) {

		delta = Gdx.graphics.getDeltaTime();

		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			personatge.x = personatge.x + Constants.PLAYER_VELOCITATX * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			personatge.x = personatge.x - Constants.PLAYER_VELOCITATX * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			personatge.y = personatge.y + Constants.PLAYER_VELOCITATY * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			personatge.y = personatge.y - Constants.PLAYER_VELOCITATY * delta;
		}
		
		if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
			float x = personatge.x + (personatge.width/2) - (Constants.DISPAR_PLAYER_AMPLE/2);
			float y = personatge.y + personatge.height;
			
			Rectangle nouDispar = new Rectangle(x, y, Constants.DISPAR_PLAYER_AMPLE, Constants.DISPAR_PLAYER_ALT);
			disparsPlayer.add(nouDispar);
		}
	}

	/*private void xoc() {
		for (Iterator iterator = enemics.iterator(); iterator.hasNext();) {
			Rectangle obstacle = (Rectangle) iterator.next();
			if (obstacle.overlaps(personatge)) {
				joc.setScreen(new GameOverScreen(joc));
				this.dispose();
				//Gdx.app.exit();
			}
		}
	}*/
	
	public void dispose () {
		super.dispose();
		personatgeTex.dispose();
	}
}
