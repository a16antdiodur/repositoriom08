package cat.iam.m8.uf3.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

import cat.iam.m8.uf3.Constants;
import cat.iam.m8.uf3.Joc;

public class SplashScreen extends ScreenAdapter {

	Texture splash;
	final Joc joc;
	
	float tempsAcumulat;
	
	public SplashScreen (Joc joc) {
		this.joc = joc;
		splash = new Texture("graphica/vaquita.jpg");
		tempsAcumulat = 0;
	}
	
	public void render(float delta) {
		super.render(delta);
		
		tempsAcumulat += delta;
		
		if (tempsAcumulat > Constants.TEMPS_ENTRE_SPLASH_GAME_SCREEN) {
			joc.setScreen(new GameScreen(joc));
			// Al cambiar de pantalla, nosotros tenemos que llamar al dispose para cerrar recursos :)
			this.dispose();
		}
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		int width = Constants.MON_AMPLE;
		int height = Constants.MON_ALT;
		
		joc.batch.begin();

		joc.batch.draw(splash, 0, 0, width, height);
				
		joc.batch.end();
	}
	
	public void dispose() {
		super.dispose();
		splash.dispose();
	}

}
