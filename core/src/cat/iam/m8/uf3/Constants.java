package cat.iam.m8.uf3;

public class Constants {

	public static final int PLAYER_AMPLE = 10;
	public static final int PLAYER_ALT = 10;
	public static final int PLAYER_POSX_INICIAL = 0;
	public static final int PLAYER_POSY_INICIAL = 0;
	public static final int PLAYER_VELOCITATX = 50;
	public static final int PLAYER_VELOCITATY = 50;
	
	public static final int TEMPS_NOU_ENEMIC = 1;
	public static final int ENEMIC_VELOCITATY = 20;
	public static final int ENEMIC_AMPLE = 10;
	public static final int ENEMIC_ALT = 10;

	public static final int TEMPS_ENTRE_SPLASH_GAME_SCREEN = 2;
	public static final int TEMPS_ENTRE_GAMEOVER_GAME = 2;
	
	public static final int DISPAR_PLAYER_AMPLE = 2;
	public static final int DISPAR_PLAYER_ALT = 6;
	public static final int DISPAR_PLAYER_VELOCITATY = 40;
	
	public static final int TEMPS_NOU_DISPAR_ENEMIC = 1;
	public static final int DISPAR_ENEMIC_VELOCITATY = 50;
	public static final int DISPAR_ENEMIC_AMPLE = 2;
	public static final int DISPAR_ENEMIC_ALT = 6;
	public static final int MON_AMPLE = 200;
	public static final int MON_ALT = 100;
	
	public static final int TEMPS_NOU_BOSS = 15;
	public static final int BOSS_AMPLE_R1 = 10;
	public static final int BOSS_AMPLE_R2 = 20;
	public static final int BOSS_AMPLE_R3 = 10;
	public static final int BOSS_ALT_R1 = 20;
	public static final int BOSS_ALT_R2 = 40;
	public static final int BOSS_ALT_R3 = 20;
	public static final int BOSS_VELOCITATX = 5;
	public static final int BOSS_VELOCITATY = 5;
	public static final int BOSS_MAX = 1;
	public static final int BOSS_XOC_DISPARS = 5;
}
