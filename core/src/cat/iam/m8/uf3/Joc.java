package cat.iam.m8.uf3;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import cat.iam.m8.uf3.screens.SplashScreen;

public class Joc extends Game {
	
	public SpriteBatch batch;
	
	// Classes libgdx per a controlar que es veu del mon
	public Viewport viewport;
	public Camera camera;
	
	@Override
	public void create () {
		batch = new SpriteBatch();

		// la camera ha de ser ortografica
		camera = new OrthographicCamera();
		// Volem que la camera apunti a la meitat del mon
		camera.position.x = Constants.MON_AMPLE / 2;
		camera.position.y = Constants.MON_ALT / 2;
		// De tots els viewport, triem el que respecta les proporcions del joc original
		// i posa la resta de la pantalla a negre.
		viewport = new FitViewport(Constants.MON_AMPLE, Constants.MON_ALT, camera);

		setScreen(new SplashScreen(this));
	}

	@Override
	public void render () {
		
		// Actualitzacio de camera
		camera.update();
		batch.setProjectionMatrix(camera.combined);

		// Neteja de pantalla
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		super.render();
	}
	
	@Override
	public void resize(int width, int height) {
		//Volem  fer el resize comu a totes les screen.
		super.resize(width, height);
		viewport.update(width, height);
	}


	@Override
	public void dispose () {
		batch.dispose();
	}
}
